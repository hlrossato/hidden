# Hidden

Hidden is a Pelican theme based and inspired by [pelican-hyde](https://github.com/jvanz/pelican-hyde) and [hyde](https://github.com/spf13/hyde).

**Social Configuration**
```python
SOCIAL = (
    ('fab', 'gitlab', 'https://gitlab.com/hlrossato'),
    ('fab', 'linkedin-in', 'https://linkedin.com/in/hlrossato'),
    ('fab', 'twitter', 'https://twitter.com/hlrossato'),
    ('fab', 'github', 'https://github.com/hlrossato'),
    ('fas', 'envelope-open', 'mailto:higorleandrorossato@gmail.com'),
)
```
![screenshot](screenshot.png)
